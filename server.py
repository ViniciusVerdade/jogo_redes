import socket
from _thread import *
import sys
import pickle
from partida import Partida

#definindo o host e port do servidor
host = "192.168.0.4"
port = 30000

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
BUFFSIZE = 2048
BIG_BUFFSIZE = 4096

#agrupando o host com a porta
try:
    s.bind((host,port))
except socket.error as e:
    str(e)

#limitando para 2 clientes apenas
s.listen(2)
print("Waiting connection...")

connected = set() #ip dos clientes conectados
games= {} #armazenara os jogos
idCount = 0 #id atual

#thread de recepção de informação do cliente
def threaded_client(conn,p,gameId):
    global idCount
    conn.send(str.encode(str(p)))
    reply=""
    while True:#3 tipos de pacote, get, reset, move
        try:
            data = conn.recv(BIG_BUFFSIZE).decode()
            if gameId in games:
                partida = games[gameId]
                if not data:
                    break
                else:
                    if data == "reset":
                        partida.resetJogada()
                    elif data != "get":
                        print(data)
                        partida.play(p, data)
                    
                    reply = partida
                    conn.sendall(pickle.dumps(reply))
            else:
                break
                
        except:
            break
    print("Lost connection")
    try:
        del games[gameId]
        print("Closing game", gameId)
    except:
        pass
    idCount -= 1
    conn.close()






while True:
    #conn = connection // addr = address do cliente
    conn, addr = s.accept()

    idCount += 1 #quantas pessoas estao conectadas ao servidor
    p = 0
    gameId = (idCount - 1)//2
    if idCount % 2 == 1:
        games[gameId] = Partida(gameId)
        print("Creating a new game...")
    else:
        games[gameId].preparado = True
        p = 1

    start_new_thread(threaded_client,(conn,p,gameId))