import socket

BUFFSIZE = 2048

class Network:
    def __init__(self) :
        self.client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        self.server = "192.168.0.4"
        self.port = 30000
        self.addr = (self.server, self.port)
        self.position = self.connect()
        
    def getPosition(self):
        return self.position

    def connect(self):
        try:
            self.client.connect(self.addr)
            return self.client.recv(BUFFSIZE).decode()
        except:
            pass
    
    def send(self, data):
        try:
            self.client.send(str.encode(data))
            return self.client.recv(BUFFSIZE).decode()
        except socket.error as e:
            print(e)