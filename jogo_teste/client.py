# 1 - Import library
import pygame
import math
import random
from network import Network

#definindo constantes e a janela do jogo
width = 700
height = 700
window = pygame.display.set_mode((width,height))

clientNumber = 0

#classe do jogador
class Player():
    def __init__ (self, x, y, width, height, color):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.color = color
        self.rect = (x,y,width,height)
        self.vel = 1
    
    #desenha o "jogador" de acordo com a posição dele
    def draw(self,window):
        pygame.draw.rect(window,self.color, self.rect)
    
    #definindo a movimentação do "jogador" de acordo
    #com os botões do teclado
    def move(self):
        keys = pygame.key.get_pressed()
        
        if keys[pygame.K_LEFT]:
            self.x -= self.vel
        if keys[pygame.K_RIGHT]:
            self.x += self.vel
        if keys[pygame.K_UP]:
            self.y -= self.vel
        if keys[pygame.K_DOWN]:
            self.y += self.vel
        self.update()
    
    def update(self):
        self.rect = (self.x,self.y,self.width,self.height)
    def setX(self, x):
        self.x = x
    def setY(self, y):
        self.y = y
        
#lendo a posição e 
#retornando na forma de inteiros o x e o y        
def read_position(str):
    str = str.split(",")
    return int(str[0]), int(str[1])

#criando uma string com as posições x e y
def make_position(tup):
    return str(tup[0]) + "," + str(tup[1])

#redesenhando a tela
def redrawWindow(window,player1,player2):
    window.fill((255,255,255))
    player1.draw(window)
    player2.draw(window)
    pygame.display.update()

def main():
    run = True
    n = Network()

    startPosition = read_position(n.getPosition()) 

    p1 = Player(startPosition[0],startPosition[1],100,100,(0,255,0))
    p2 = Player(0,0,100,100,(0,0,255))

    clock = pygame.time.Clock()

    while run:
        clock.tick(60)   

        p2Pos = read_position(n.send(make_position( (p1.x,p1.y) )))
        p2.x = p2Pos[0]
        p2.y = p2Pos[1]
        p2.update()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()
        p1.move()
        redrawWindow(window,p1,p2)

main()