import socket
from _thread import *
import sys

#definindo o host e port do servidor
host = "192.168.0.4"
port = 30000

sserver = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
BUFFSIZE = 2048

#agrupando o host com a porta
try:
    sserver.bind((host,port))
except socket.error as e:
    str(e)

#limitando para 2 clientes apenas
sserver.listen(2)
print("Waiting connection...")

#lendo a posição e 
#retornando na forma de inteiros o x e o y        
def read_position(str):
    str = str.split(",")
    return int(str[0]), int(str[1])

#criando uma string com as posições x e y
def make_position(tup):
    return str(tup[0]) + "," + str(tup[1])

pos = [(0,0),(100,100)]

#thread de recepção de informação do cliente
def threaded_client(conn,player):
    conn.send(str.encode(make_position( pos[player] )))
    reply = ""

    while True:
        try:
            data = read_position( conn.recv(BUFFSIZE).decode() )
            pos[player] = data


            if not data:
                print("Disconnected")
                break
            else:
                if player == 1:
                    reply = pos[0]
                else:
                    reply = pos[1]
                print("Received: ", data)
                print("Sending: ",reply)
            
            conn.sendall(str.encode(make_position(reply)))
        except:
            break
    print("Lost connection")
    conn.close()



#checando conecções
currentPlayer = 0
while True:
    #conn = connection // addr = address do cliente
    conn, addr = sserver.accept()
    print("Conectado a:",addr)

    start_new_thread(threaded_client,(conn,currentPlayer))
    #toda vez que aceita uma conexão, mais um jogador
    currentPlayer += 1
    
