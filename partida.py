PEDRA = 0
PAPEL = 1
TESOURA = 2
class Partida:
    def __init__(self,id):
        self.J1Jogou = False
        self.J2Jogou = False
        self.preparado = False
        self.id = id
        self.jogadas = [None,None]
        self.vitorias = [0,0]
        self.empates = 0

    def get_jogada_jogador(self,p):
        #vai receber uma jogada [0,1]
        return self.jogada[p]
    def play(self,jogador,jogada):
        self.jogadas[jogador] = jogada
        if jogador == 0:
            self.J1Jogou = True
        else:
            self.J2Jogou = True

    def connected(self):
        return self.preparado

    def ambosJogaram(self):
        return self.J1Jogou and self.J2Jogou

    def vitorioso(self):
        p1 = int(self.jogadas[0])
        p2 = int(self.jogadas[1])

        vencedor = -1
        if p1 == PEDRA and p2 == TESOURA:
            vencedor = 0
        elif p1 == PEDRA and p2 == PAPEL:
            vencedor = 1
        elif p1 == TESOURA and p2 == PAPEL:
            vencedor = 1
        elif p1 == TESOURA and p2 == PEDRA:
            vencedor = 0
        elif p1 == PAPEL and p2 == PEDRA:
            vencedor = 0
        elif p1 == PAPEL and p2 == TESOURA:
            vencedor = 1

        return vencedor
        
    def resetJogada(self):
        self.J1Jogou = False
        self.J2Jogou = False