import pygame
from jogador import Jogador
from network import Network
import time
from pygame.locals import *

#definindo constantes
PEDRA = 0
PAPEL = 1
TESOURA = 2
COMUM = 3

MENU_PRINCI = 0
TELA_NOMEACAO = 1
TELA_CARREGAMENTO = 2
TELA_JOGO = 3

BUFFSIZE = 2048


#inicializando o partida
pygame.init() 

#definindo janela do jogo
tela_menu_principal = pygame.image.load("resources/menu_principal.png")
pos_tela_menu_principal = (0,0)

width = tela_menu_principal.get_width()
height = tela_menu_principal.get_height()

screen = pygame.display.set_mode((width,height))

tela_menu_nomeacao = pygame.image.load("resources/menu_nomeacao.png")
pos_tela_menu_nomeacao = (0,0)

tela_menu_carregamento = pygame.image.load("resources/menu_carregamento.png")
pos_tela_menu_carregamento = (0,0)

tela_menu_jogo = pygame.image.load("resources/menu_jogo.png")
pos_tela_menu_jogo = (0,0)

#definindo o botao jogar
botao_jogar = pygame.image.load("resources/botao_jogar.png")
pos_botao_jogar = (189,450)
botao_jogar_rect = pygame.Rect(botao_jogar.get_rect())
botao_jogar_rect.top = 450
botao_jogar_rect.left = 189

#definindo o botao aceitar
botao_aceitar = pygame.image.load("resources/botao_aceitar.png")
pos_botao_aceitar = (189,450)
botao_aceitar_rect = pygame.Rect(botao_aceitar.get_rect())
botao_aceitar_rect.top = 450
botao_aceitar_rect.left = 189

#definindo os botoes de escolha
botoes_escolha=[pygame.image.load("resources/botao_pedra.png"),
                pygame.image.load("resources/botao_papel.png"),
                pygame.image.load("resources/botao_tesoura.png")]
pos_botoes_escolha = [(100,450),
                      (280,450),
                      (460,450)]
botoes_escolha_rect = [pygame.Rect(botoes_escolha[PEDRA].get_rect()),
                       pygame.Rect(botoes_escolha[PAPEL].get_rect()),
                       pygame.Rect(botoes_escolha[TESOURA].get_rect())]
botoes_escolha_rect[PEDRA].top = 450
botoes_escolha_rect[PEDRA].left = 100

botoes_escolha_rect[PAPEL].top = 450
botoes_escolha_rect[PAPEL].left = 280

botoes_escolha_rect[TESOURA].top = 450
botoes_escolha_rect[TESOURA].left = 460

#definindo os sprites dos jogadores
homem = [pygame.image.load("resources/homem_pedra.png"),
                pygame.image.load("resources/homem_papel.png"),
                pygame.image.load("resources/homem_tesoura.png"),
                pygame.image.load("resources/homem_comum.png")]
mulher = [pygame.image.load("resources/mulher_pedra.png"),
                pygame.image.load("resources/mulher_papel.png"),
                pygame.image.load("resources/mulher_tesoura.png"),
                pygame.image.load("resources/mulher_comum.png")]
pos_homem = (-50,100)
pos_mulher = (350,100)

#definindo telas vitoria e derrota
vitoria = pygame.image.load("resources/JPEG/tela_vitoria.jpg")
pos_vitoria = (50,50)

derrota = pygame.image.load("resources/JPEG/tela_derrota.jpg")
pos_derrota = (50,50)


#box nomeaçao
box_nomeacao = pygame.image.load("resources/box_nome.png")
pos_box_nome = (67,225)
box_nomeacao_rect = pygame.Rect(box_nomeacao.get_rect())
box_nomeacao_rect.top = 225
box_nomeacao_rect.left = 67

#carregando as resources do jogo
def printar_menu_principal(window):
    screen.blit(tela_menu_principal,pos_tela_menu_principal)
    screen.blit(botao_jogar, pos_botao_jogar)

def printar_menu_nomeacao(window):
    screen.blit(tela_menu_nomeacao,pos_tela_menu_nomeacao)
    screen.blit(box_nomeacao, pos_box_nome)
    screen.blit(botao_aceitar, pos_botao_aceitar)

def printar_menu_carregamento(window):
    screen.blit(tela_menu_carregamento,pos_tela_menu_carregamento)

def printar_jogo_escolhendo(window,text1,text2):
    font = pygame.font.Font(None, 40)
    

    screen.blit(tela_menu_jogo,pos_tela_menu_jogo)
    screen.blit(homem[COMUM],pos_homem)
    screen.blit(mulher[COMUM],pos_mulher)
    screen.blit(botoes_escolha[PEDRA],pos_botoes_escolha[PEDRA])
    screen.blit(botoes_escolha[PAPEL],pos_botoes_escolha[PAPEL])
    screen.blit(botoes_escolha[TESOURA],pos_botoes_escolha[TESOURA])
    
    txt_surface1 = font.render(text1, True, (0,0,0))
    txt_surface2 = font.render(text2, True, (255,0,0))
    screen.blit(txt_surface1, (200,300))
    screen.blit(txt_surface2, (200,360))

def printar_jogo_escolhido(window,escolha1,escolha2):
    screen.blit(tela_menu_jogo,pos_tela_menu_jogo)
    screen.blit(homem[int(escolha1)],pos_homem)
    screen.blit(mulher[int(escolha2)],pos_mulher)    

#ROTINA DO MENU PRINCIPAL:
#FICA LOOPANDO ENQUANTO O JOGADOR NÃO CLICA EM JOGAR
def rotina_menu_principal(run, screen):
    printar_menu_principal(screen)
    telaAtual = 0
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
            pygame.quit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            mouse_position = event.pos
            if botao_jogar_rect.collidepoint(mouse_position):
                telaAtual = TELA_NOMEACAO
                break
    pygame.display.update()
    return run, telaAtual

#ROTINA DA NOMEAÇÃO:
#ESPERA O JOGADOR CLICAR NA CAIXA DE TEXTO, DIGITAR SEU NOME E
#ACEITAR O NOME DIGITADO
def rotina_nomeacao(run,screen,entry,_text,_jogador,network):
    printar_menu_nomeacao(screen)
    telaAtual = 1
    active = entry
    jogador = _jogador
    text = _text
    font = pygame.font.Font(None, 32)
    txt_surface = font.render(text, True, (255,0,0))
    screen.blit(txt_surface, (box_nomeacao_rect.x+10, box_nomeacao_rect.y+50))

    for event in pygame.event.get():
        
        if event.type == pygame.QUIT:
            run = False
            pygame.quit()

        if event.type == pygame.MOUSEBUTTONDOWN:
            mouse_position = event.pos
            
            if box_nomeacao_rect.collidepoint(mouse_position):
                active = True
            else:
                active = False

            if botao_aceitar_rect.collidepoint(mouse_position):
                jogador = Jogador(text)
                #str(network.addr) + "/" 
                #network.connect( jogador.nome)
                telaAtual = TELA_JOGO
                break
            
        if event.type == pygame.KEYDOWN:
            if active:
                if event.key == pygame.K_BACKSPACE:
                    text = text[:-1]
                else:
                    text += event.unicode
        
        pygame.display.flip()

    pygame.display.update()
    return run, telaAtual, active, text, jogador

#ROTINA DO CARREGAMENTO:
#CONECTA COM O SERVIDOR (QUE DEVE ESTAR ABERTO)
#E ESPERA SEGUNDO JOGADOR CONECTAR

def rotina_jogo(run, screen, jogador,network,player,vida_players):

    #printar_menu_carregamento(screen)
    #telaAtual = TELA_CARREGAMENTO

    try:
        partida = network.send("get" )#requisita partida para o servidor
    except:
        run = False #caso não haja partida, quebra 
        print("O seu jogo não foi encontrado...")
        #break

    #quando o nome estiver funcionando, srá pra colocar player como parametro do vetor de nome de player
    if(player == 0):
        nome_vida1 = 'Você: ' + str(vida_players[player])
        nome_vida2 = 'Oponente: ' + str(vida_players[1])
    else:
        nome_vida1 = 'Você: ' + str(vida_players[player])
        nome_vida2 = 'Oponente: ' + str(vida_players[0])

    #se não houver jogadores o suficiente no jogo, fica na tela de carregamento, se não, printa a tela de escolher opções
    if not(partida.connected()):
        printar_menu_carregamento(screen)
        pygame.display.update()
    else:
        printar_jogo_escolhendo(screen,nome_vida1,nome_vida2)
        pygame.display.update()

    #caso ambos os jogadores tenham jogado, reseta o jogo, ou seja, reseta a escolha de cada jogador, atualiza as vidas e checa vitoria ou derrota
    if partida.ambosJogaram():
        try:
            partida = network.send("reset")
        except:
            run = False
            print("O seu jogo não foi encontrado...")
            #break

        if(partida.vitorioso() == 1 and player == 1 ) or (partida.vitorioso() == 0 and player == 0 ):
            #tira vida do oponente
            if player == 1:
                vida_players[0] -= 1
            else:
                vida_players[1] -= 1

        elif partida.vitorioso() == -1:
            #empatou
            pass
        else:
            #tira a vida do jogador
            vida_players[player] -= 1
        
        #caso alguem tenha ficado com 0 de vida, apresenta tela de vitoria ou derrota
        if player == 1:
            if vida_players[0] == 0:
                run = False
                screen.blit(vitoria,pos_vitoria)
                pygame.display.update()
                time.sleep(3)
                #vitoria
            elif vida_players[1] ==0:
                run = False
                screen.blit(derrota,pos_derrota)
                pygame.display.update()
                time.sleep(3)
                #derrota
        else:
            if vida_players[1] == 0:
                run = False
                screen.blit(vitoria,pos_vitoria)
                pygame.display.update()
                time.sleep(3)
                #vitoria
            elif vida_players[0] == 0:
                run = False
                screen.blit(derrota,pos_derrota)
                pygame.display.update()
                time.sleep(3)
                #derrota
        #blita durante 2 segundos a "jogada", ou seja, as assets dos personagens fazendo suas escolhas
        if player == 1 and run:
            print(partida.jogadas[player])
            printar_jogo_escolhido(screen,partida.jogadas[player],partida.jogadas[0])
            pygame.display.update()
            time.sleep(2)
        elif player == 0 and run:
            print(partida.jogadas[player])
            printar_jogo_escolhido(screen,partida.jogadas[player],partida.jogadas[1])
            pygame.display.update()
            time.sleep(2)

    #checando se o jogador clicou no botão, caso ja tenha clicado uma vez, não manda o pacote
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
            pygame.quit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            mouse_position = event.pos
            if botoes_escolha_rect[PEDRA].collidepoint(mouse_position):
        
                jogador.escolha = PEDRA
                if player == 0:
                    if not partida.J1Jogou:
                        network.send(PEDRA)
                else:
                    if not partida.J2Jogou:
                        network.send(PEDRA)
        
            if botoes_escolha_rect[PAPEL].collidepoint(mouse_position):
            
                jogador.escolha = PAPEL
                
                if player == 0:
                    if not partida.J1Jogou:
                        network.send(PAPEL)
                else:
                    if not partida.J2Jogou:
                        network.send(PAPEL)
                    
            if botoes_escolha_rect[TESOURA].collidepoint(mouse_position):
                
                jogador.escolha = TESOURA
                if player == 0:
                    if not partida.J1Jogou:
                        network.send(TESOURA)
                else:
                    if not partida.J2Jogou:
                        network.send(TESOURA)
        
    return run, vida_players

def main():
    run = True
    active = False
    
    text = ''
    jogador = None

    n = Network()
    player = int(n.getP())
    vida_players = [3,3]
    print("Você é o jogador ",player)
    telaAtual = 0
    
    while run:
        
        if telaAtual == MENU_PRINCI:
            run, telaAtual = rotina_menu_principal(run, screen)
        elif telaAtual == TELA_NOMEACAO:
            run, telaAtual, active, text, jogador = rotina_nomeacao(run, screen,active,text,jogador,n)
        elif telaAtual == TELA_JOGO:
            run, vida_players = rotina_jogo(run, screen,jogador,n,player,vida_players)

main()