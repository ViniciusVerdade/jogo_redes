PEDRA = 0
PAPEL = 1
TESOURA = 2

class Jogador():
    def __init__(self,nome):
        self.nome = nome
        self.vida = 3
        self.escolha = -1
        self.id = -1
    
    def fazerEscolha(self,escolha):
        if escolha == PEDRA:
            self.escolha = PEDRA
        if escolha == PAPEL:
            self.escolha = PAPEL
        if escolha == TESOURA:
            self.escolha = TESOURA
    
    def perderVida(self):
        self.vida -= 1

